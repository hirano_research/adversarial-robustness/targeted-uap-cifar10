from keras import utils, Model
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Input
from keras.optimizers import Adam, SGD
from keras.callbacks import LearningRateScheduler, TensorBoard, ModelCheckpoint
import numpy as np
import time
import pickle

import sys, pdb
sys.path.append("CNN-CIFAR10")
sys.path.append("adversarial-robustness-toolbox")
import vgg_model
from art.classifiers import KerasClassifier
from art.attacks import UniversalPerturbation

num_classes = 10
num_blocks = 3
target = 0

# load data
(x_train, y_train), (x_test, y_test) = cifar10.load_data()
# norm2_mean = 0
# for im in x_train:
#     norm2_mean += np.linalg.norm(im.flatten(), ord=2)
# norm2_mean /= x_train.shape[0]
norm2_mean = 7380.994660106816

# normalize data
channel_mean = np.mean(x_train, axis=(0, 1, 2))
channel_std = np.std(x_train, axis=(0, 1, 2))
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

for i in range(3):
    x_train[:, :, :, i] = (x_train[:, :, :, i] - channel_mean[i]) / channel_std[i]
    x_test[:, :, :, i] = (x_test[:, :, :, i] - channel_mean[i]) / channel_std[i]

# labels to categorical
y_train = utils.to_categorical(y_train, num_classes)
y_train = utils.to_categorical(y_test, num_classes)

# trainから10000枚
idx_list = []
for class_i in range(10):
    np.random.seed(111)
    idx = np.random.choice( np.where(y_train[:, class_i]==1)[0], 100, replace=False ).tolist()
    idx_list.append(idx)
trainIdx = np.array(idx_list)
trainIdx = np.random.permutation(trainIdx)
x_train, y_train = x_train[trainIdx], y_train[trainIdx]

# build resnext model
img_input = Input(shape=(32, 32, 3), name='input')
img_prediction = vgg_model.vgg_model(img_input, num_classes, num_blocks)
model = Model(img_input, img_prediction)
model.load_weights("CNN-CIFAR10/model/vgg_20_1553198546.h5")

# targeted UAP
classifier = KerasClassifier(model=model)
adv_crafter = UniversalPerturbation(
    classifier,
    attacker='fgsm',
    delta=0.000001,
    attacker_params={'targeted':True, 'eps':0.006},
    max_iter=2,
    eps=0.16,
    norm=np.inf)

y_train_adv_tar = np.zeros(y_train.shape)
for i in range(y_train.shape[0]):
    y_train_adv_tar[i, target] = 1.0

x_train_adv = adv_crafter.generate(x_train, y=y_train_adv_tar, targeted=True)

noise = adv_crafter.noise[0,:]
np.save('noise.npy', noise)

x_test_adv = x_test + noise
